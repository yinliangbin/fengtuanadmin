// import { combineReducers, createStore } from "redux";
import { createStore } from "redux";
import { combineReducers } from "redux-immutable";

import example from "../pages/example/store";

export default createStore(
  combineReducers({
    // login: reducer,
    example,
  })
);
